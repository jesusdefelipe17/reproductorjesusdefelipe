package com.jmcompany.jesus.reproductor;

import android.Manifest;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;

public class Grabadora extends AppCompatActivity {
    private static final int MY_PERMISSIONS_REQUEST_MIC = 1;
    private static final String TAG = "grabar";
    private  MediaRecorder mr;
    private ImageView grabadora;
    private ImageView apagar_grabadora;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grabadora);
        setTitle("Grabadora");
    }
    public void permisosMicro(View view){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO,Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_MIC);



        }else{

            grabar();

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {

            case MY_PERMISSIONS_REQUEST_MIC: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.



                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.


                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }



    public void grabar(){

        grabadora = findViewById(R.id.grabadora);
        apagar_grabadora = findViewById(R.id.apagar_grabadora);

        grabadora.setVisibility(View.GONE);
        apagar_grabadora.setVisibility(View.VISIBLE);

        final String SONG = "song_record.3gp";

        final String PATH_SONG = Environment.getExternalStorageDirectory().toString() + File.separator + Environment.DIRECTORY_MUSIC + File.separator + SONG;

        File f = new File(PATH_SONG);
        Log.d(TAG, PATH_SONG);



        if (f.exists()){

                f.delete();

        }
                 mr = new MediaRecorder();
                 mr.setAudioSource(MediaRecorder.AudioSource.MIC);
                 mr.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                 mr.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
                 mr.setOutputFile(PATH_SONG);


        try {
            mr.prepare();
        }
        catch (IOException e){
            e.printStackTrace();
            Log.d(TAG, "HOLA");
        }

        mr.start();


    }


    public void Parar_grabar(View view) {
        grabadora = findViewById(R.id.grabadora);
        apagar_grabadora = findViewById(R.id.apagar_grabadora);

        apagar_grabadora.setVisibility(View.GONE);
        grabadora.setVisibility(View.VISIBLE);


        if (mr != null){
            mr.stop();
        }

    }
}