package com.jmcompany.jesus.reproductor;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.File;
import java.util.concurrent.TimeUnit;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class Principal extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "cancion";
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    private ImageView play, stop, continuar, atras, siguiente, pausar, archivos, disco, internet;
    private SeekBar seekBar;
    private Handler handler;
    private Runnable runnable;
    private Uri uri = null;
    private MediaPlayer mp;
    private MediaRecorder mr;
    private Animation rotateAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        play = findViewById(R.id.play);
        stop = findViewById(R.id.parar);
        continuar = findViewById(R.id.continuar);
        atras = findViewById(R.id.atras);
        siguiente = findViewById(R.id.siguiente);
        pausar = findViewById(R.id.pausar);
        archivos = findViewById(R.id.archivos);
        disco = findViewById(R.id.disco);
        internet = findViewById(R.id.internet);
        mp = new MediaPlayer();
        play.setEnabled(false);
        seekBar = findViewById(R.id.seekBar);

        rotateAnimation = AnimationUtils.loadAnimation(this, R.anim.animacion_rotate);
        mp = MediaPlayer.create(this, R.raw.unlike);


        handler= new Handler();

        setTitle("Musica");


    }

    private void changeSneekBar() {
        seekBar.setProgress(mp.getCurrentPosition());
        if(mp.isPlaying()){
            runnable = new Runnable() {
                @Override
                public void run() {
                    changeSneekBar();
                }
            };
            handler.postDelayed(runnable,1000);
        }
    }

    public void song(){

        mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                seekBar.setMax(mp.getDuration());
                mp.start();
                changeSneekBar();
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser){
                    mp.seekTo(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {

            Intent intent = new Intent(this, Video.class);
            startActivity(intent);

        } else if (id == R.id.nav_gallery) {

            Intent intent = new Intent(this, EleccionFotoOVideo.class);
            startActivity(intent);

        } else if (id == R.id.nav_slideshow) {
            Intent intent = new Intent(this, Grabadora.class);
            startActivity(intent);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void siguiente(View view) {
        final int time = 5000;
        if (mp != null && mp.isPlaying()) {
            if (mp.getCurrentPosition() + time >= mp.getDuration()) {
                mp.seekTo(mp.getDuration());
            }
            mp.seekTo(mp.getCurrentPosition() + time);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == 1) {

            if (resultCode == RESULT_OK) {

                //El uri del audio seleccionado
                uri = data.getData();

                Log.d(TAG, uri.toString());

                Intent intent = new Intent(this, Principal.class);
                intent.putExtra(TAG, uri.toString());


                mp = MediaPlayer.create(this, uri);



                /*
                play.setVisibility(View.GONE);
                archivos.setEnabled(false);
                pausar.setVisibility(View.VISIBLE);
                play.setEnabled(true);
                */
            }
        } else {

        }


    }


    public void play(View view) {


        internet.setEnabled(false);

        disco.startAnimation(rotateAnimation);


        if (mp != null && uri == null) {

        } else {
            mp = MediaPlayer.create(this, uri);
            mp.start();
            song();


        }


        play.setVisibility(View.GONE);

        pausar.setVisibility(View.VISIBLE);


    }

    public void atras(View view) {
        final int time = 5000;
        if (mp != null && mp.isPlaying()) {
            if (mp.getCurrentPosition() - time >= mp.getDuration()) {
                mp.seekTo(mp.getDuration());
            }
            mp.seekTo(mp.getCurrentPosition() - time);
        }
    }

    public void stop(View view) {

        pausar.setVisibility(View.GONE);
        continuar.setVisibility(View.GONE);

        play.setVisibility(View.VISIBLE);
        internet.setEnabled(true);

        archivos.setEnabled(true);
        if (mp != null && mp.isPlaying()) {
            mp.stop();


            rotateAnimation.cancel();

        }




    }

    public void pausa(View view) {
        pausar.setVisibility(View.GONE);
        continuar.setVisibility(View.VISIBLE);

        if (mp != null && mp.isPlaying()) {
            mp.pause();
            play.setEnabled(true);
            rotateAnimation.cancel();
        }
    }

    public void continuar(View view) {

        continuar.setVisibility(View.GONE);
        pausar.setVisibility(View.VISIBLE);

        if (mp != null) {
            mp.start();
            rotateAnimation.reset();
            disco.startAnimation(rotateAnimation);
            changeSneekBar();
        }
    }

    public void permissos() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?


            // No explanation needed, we can request the permission.

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS);

            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.


        } else {

            Intent intent = new Intent(MediaStore.INTENT_ACTION_MUSIC_PLAYER);
            Uri uri = Uri.parse(Environment.getExternalStorageDirectory().toString() + File.separator + Environment.DIRECTORY_MUSIC + File.separator);
            intent.setDataAndType(uri, "audio/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, 1);

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public void gestorArchivos(View view) {

        pausar.setVisibility(View.GONE);
        continuar.setVisibility(View.GONE);

        play.setVisibility(View.VISIBLE);
        play.setEnabled(true);
        internet.setEnabled(false);

        archivos.setEnabled(true);
        if (mp != null && mp.isPlaying()) {
            mp.stop();
        }
        Intent intent = new Intent(MediaStore.INTENT_ACTION_MUSIC_PLAYER);
        Uri uri = Uri.parse(Environment.getExternalStorageDirectory().toString() + File.separator + Environment.DIRECTORY_MUSIC + File.separator);
        intent.setDataAndType(uri, "audio/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, 1);






    }

    @Override
    protected void onDestroy() {//Cuando te sales de la aplicacion para
        super.onDestroy();
        mp.stop();
    }


    public void playInternet(View view) {

        final String AUDIO_PATH = "https://mm-mobileapps.com/mujeres_vino.mp3";

        Uri uri = Uri.parse(AUDIO_PATH);

        mp = MediaPlayer.create(this, uri);

        Log.d(TAG, uri.toString());

        mp.start();

        internet.setEnabled(false);
        play.setVisibility(View.GONE);
        pausar.setVisibility(View.VISIBLE);
        song();
        disco.startAnimation(rotateAnimation);
    }



}




