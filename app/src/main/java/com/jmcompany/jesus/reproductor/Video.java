package com.jmcompany.jesus.reproductor;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

public class Video extends AppCompatActivity {
    private static final String EXTRA_MESSAGE = "";
    private static final String TAG = "cancion";
    private SurfaceView surfaceView=null;
    private  ImageView play, stop, continuar, atras, siguiente, pausar,archivos;
    private MediaPlayer mp;
    private Uri uri = null;
    private String message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        surfaceView = findViewById(R.id.surfaceView);


        Intent intent = getIntent();
        message = intent.getStringExtra(Video.EXTRA_MESSAGE);

        // Capture the layout's TextView and set the string as its text
        play = findViewById(R.id.play);
        stop = findViewById(R.id.parar);
        continuar = findViewById(R.id.continuar);
        atras = findViewById(R.id.atras);
        siguiente = findViewById(R.id.siguiente);
        pausar = findViewById(R.id.pausar);
        archivos = findViewById(R.id.archivos);

        play.setEnabled(false);
        stop.setEnabled(false);
        setTitle("Video");


    }

    public void siguiente(View view) {
        final int time = 5000;
        if (mp != null && mp.isPlaying()) {
            if (mp.getCurrentPosition() + time >= mp.getDuration()) {
                mp.seekTo(mp.getDuration());
            }
            mp.seekTo(mp.getCurrentPosition() + time);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == 1) {

            if (resultCode == RESULT_OK) {

                //El uri del audio seleccionado
                uri = data.getData();

                Log.d(TAG, uri.toString());

                Intent intent = new Intent(this, Principal.class);
                intent.putExtra(TAG, uri.toString());


                play.setEnabled(true);
                stop.setEnabled(true);

                /*
                if(mp==null){
                    mp = MediaPlayer.create(this,uri);
                    mp.setDisplay(surfaceView.getHolder());//Definimos donde se va a mostrar el mediaplayer
                }

                mp.start();
                */





            }
        }


    }



    public void play(View view) {


    if(mp==null) {
        mp = MediaPlayer.create(this, uri);
        mp.setDisplay(surfaceView.getHolder());//Definimos donde se va a mostrar el mediaplayer
       mp.start();
    }else {
        mp = MediaPlayer.create(this, uri);
        mp.setDisplay(surfaceView.getHolder());//Definimos donde se va a mostrar el mediaplayer
        mp.start();
    }

        play.setVisibility(View.GONE);

        pausar.setVisibility(View.VISIBLE);

        archivos.setEnabled(false);





    }

    public void atras(View view) {
        final int time = 5000;
        if (mp != null && mp.isPlaying()) {
            if (mp.getCurrentPosition() - time >= mp.getDuration()) {
                mp.seekTo(mp.getDuration());
            }
            mp.seekTo(mp.getCurrentPosition() - time);
        }
    }

    public void stop(View view) {

        pausar.setVisibility(View.GONE);
        continuar.setVisibility(View.GONE);

        play.setVisibility(View.VISIBLE);
        archivos.setEnabled(true);


        if (mp != null){
            mp.pause();
            mp.seekTo(0);
            SurfaceHolder surfaceHolder =null ;
            mp.setDisplay(surfaceHolder);
        }

    }

    public void pausa(View view) {
        pausar.setVisibility(View.GONE);
        continuar.setVisibility(View.VISIBLE);

        if (mp != null && mp.isPlaying()) {
            mp.pause();
            play.setEnabled(true);
        }
    }

    public void continuar(View view) {

        continuar.setVisibility(View.GONE);
        pausar.setVisibility(View.VISIBLE);

        if (mp != null) {
            mp.start();
        }
    }
    public void gestorArchivos(View view) {


        Intent intent = new Intent(MediaStore.INTENT_ACTION_VIDEO_PLAY_FROM_SEARCH);
        Uri uri = Uri.parse(Environment.getExternalStorageDirectory().toString() + File.separator + Environment.DIRECTORY_MUSIC + File.separator);
        intent.setDataAndType(uri, "video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,1);



    }

}
